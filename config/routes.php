<?php
return array(
    
    'news/([0-9]+)' => 'news/view/$1',

    //'news/([a-z]+)/([0-9]+)' => 'news/view/$1/$2',  // ex. news/sport/114
	                      // echo "Foreach Works in Router: Array this->routes as Key uriPattern => Value path";
						  // Here is: Key uriPattern => Value path;
	
	//'news/([0-9]+)' => 'news/view',
	/*
    'news/55' => 'news/view',
	'news/77' => 'news/view',
    */

	'news' => 'news/index', // actionIndex in NewsController, request - controller/action


	// 'products' => 'product/list', // actionList in ProductController, request - controller/action
);