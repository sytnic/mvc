<?php

class Db
{ 
    
    public static function getConnection() {
        $paramsPath = ROOT . '/config/db_params.php';
		$params = include($paramsPath); // array with db parameters

		$dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        // необязательно в две строки
		$db = new PDO($dsn, $params['user'], $params['password']);

		return $db; // object

    }




}








?>