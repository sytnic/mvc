<?php

class Router
{

	private $routes; // array

	public function __construct()
	{
		$routesPath = ROOT.'/config/routes.php';
     	$this->routes = include($routesPath);
	}

	// returns string request
	private function getURI()
	{
		if (!empty($_SERVER['REQUEST_URI'])) {
		    return trim($_SERVER['REQUEST_URI'], '/');
		}

	}

    public function run()
	{
		// Получить строку запроса
		$uri = $this->getURI();
			//echo $uri;

		// Проверить наличие запроса в роутах
		foreach ($this->routes as $uriPattern => $path) {
				// echo "<br> $uriPattern => $path";

			// если $uriPattern из routes содержится в строке запроса
			if(preg_match("~$uriPattern~", $uri)) {
					//echo "$path";
					/*
					echo "Foreach Works in Router: Array this->routes as Key uriPattern => Value path";
					echo "<br>Где ищем (запрос, который набрал пользователь) uri (взят из s_SERVER): ".$uri;
					echo "<br>Что ищем (совпадение из правила) uriPattern (взят из routes): ".$uriPattern;
					echo "<br>Кто обрабатывает path (взят из routes): ".$path; 
					*/
				// Получаем внутренний путь из внешнего согласно правилу.

			$internalRoute = preg_replace("~$uriPattern~", $path, $uri);

				// echo '<br>Получен внутренний путь (без корня): '.$internalRoute.'<br>'; // путь вбитый в адр. строку кроме root/,
				// ex. news/view/sport/114, можно определить соответственно controller/action/parameters

				// Определить какой контроллер, action and parameters


				// полученный внутренний путь разбивается на массив составляющих
				$segments = explode('/', $internalRoute);
					// print_r($segments);

				//выдергиваем первый элемент, он есть нужный контроллер
				$controllerName = array_shift($segments).'Controller';
				$controllerName = ucfirst($controllerName);
					//echo $controllerName;

				// определяем следующий элемент, он есть action
				$actionName = 'action'.ucfirst((array_shift($segments)));
					//echo $actionName;
                    
					//echo "Класс: ".$controllerName."<br>";  // ex. NewsController or ProductController
					//echo "Метод: ".$actionName;             // ex. actionIndex    or actionList
				
				// остались параметры в массиве segments
				$parameters = $segments;

					//print_r($parameters);


				// Подключить файл класса-контроллера
				$controllerFile = ROOT . '/controllers/' .$controllerName. '.php';
				if (file_exists($controllerFile)) {
					include_once($controllerFile);
				}

				// Создать объект контроллера, 
				$controllerObject = new $controllerName;     // ex. new ProductController; // new object
				// вызвать метод (action) с параметрами запроса, ex. sport/121, parameters [0 => sport, 1 => 114]
				
				//$result = $controllerObject->$actionName($parameters);  // ex. $object->actionList($array); // method launch 
				// or
				$result = call_user_func_array(array($controllerObject, $actionName), $parameters);
				    if ($result != null) {
						break;
					}

			}	
			
		}

	}

}